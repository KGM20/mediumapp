/* eslint-disable no-undef */
const mongoose = require('mongoose');

const ArticleSchema = new mongoose.Schema({
  text: String,
  title: String,
  description: String,
  feature_img: String,
  claps: Number,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  comments: [
    {
      author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
      text: String,
    },
  ],
});

ArticleSchema.methods.clap = () => {
  this.claps += 1;
  return this.save();
};

ArticleSchema.methods.comment = comment => {
  this.comments.push(comment);
  return this.save();
};

ArticleSchema.methods.addAuthor = authorId => {
  this.author = authorId;
  return this.save();
};

ArticleSchema.methods.getUserArticle = _id => {
  Article.find({ author: _id }).then(article => {
    return article;
  });
};

module.exports = mongoose.model('Article', ArticleSchema);
