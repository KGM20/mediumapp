import logo from './logo.svg';

const App = () => {
  return (
    <div className="text-center">
      <header className="App-header bg-slate-900 min-h-screen flex flex-col items-center justify-center text-2xl text-white">
        <img src={logo} className="h-[40vmin] pointer-events-none animate-spin-slow" alt="logo" />
        <p className="text-fuchsia-900">
          Edit
          <code>src/App.js</code>
          and save to reload.
        </p>
        <a
          className="text-sky-300"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
};

export default App;
